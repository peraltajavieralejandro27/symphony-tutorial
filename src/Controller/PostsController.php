<?php

namespace App\Controller;

use App\Entity\Posts;
use App\Form\PostsType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;

class PostsController extends AbstractController
{
    /**
     * @Route("/posts/register", name="posts-register")
     * @throws Exception
     */
    public function index(Request $request): Response
    {
        $post = new Posts();
        $form = $this->createForm(PostsType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $photoFile = $form->get('photo')->getData();

            if ($photoFile) {
                $originalFilename = pathinfo($photoFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = (new AsciiSlugger())->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $photoFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $photoFile->move(
                        $this->getParameter('photos_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                    throw new Exception('And error has ocurred while uploading a file');
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $post->setPhoto($newFilename);
            }


            $authUser = $this->getUser();
            $post->setUsers($authUser);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirectToRoute('dashboard');
        }

        return $this->render('posts/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }



    /**
     * @Route("/posts_my", name="posts-my")
     * @throws Exception
     */

    public function myPosts()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $posts = $entityManager->getRepository(Posts::class)->findBy(['users' => $user]);

        return $this->render('posts/my_posts.html.twig', compact('posts'));
    }

    /**
     * @Route("/posts/{id}", name="posts-show")
     * @throws Exception
     */
    public function show($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $post = $entityManager->getRepository(Posts::class)->findOneBy(['id' => $id]);

        return $this->render('posts/show.html.twig', compact('post'));
    }
}
